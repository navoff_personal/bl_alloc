/**
 * @file bl_allocator_test.c
 * @author Постнов В.М.
 * @date 20 янв. 2020 г.
 * @brief
 * @addtogroup
 * @{
*/

#include <stdbool.h>
#include <stddef.h>

#include "bl_allocator.h"

// ========================================= Definition ============================================


// ========================================= Declaration ===========================================


// ======================================== Implementation =========================================

bool bl_test_chunk_amount(bl_allocator_t *bl_allocator)
{
  size_t chunk_count = 0;

  while (1)
  {
    uint8_t *buf = bl_allocate(bl_allocator);
    if (buf == NULL)
    {
      break;
    }

    chunk_count++;
    if (chunk_count > BL_POOL_SIZE)
    {
      return false;
    }
  }

  if (chunk_count != BL_POOL_SIZE)
  {
    return false;
  }

  return true;
}

bool bl_test_alloc(bl_allocator_t *bl_allocator)
{
  uint8_t *buffers[BL_POOL_SIZE];

  // alloc all chunks and fill by sample
  for (size_t i = 0; i < BL_POOL_SIZE; i++)
  {
    buffers[i] = bl_allocate(bl_allocator);

    for (size_t k = 0; k < BL_CHUNK_SIZE; k++)
    {
      buffers[i][k] = i + k;
    }
  }

  // compare with sample
  for (size_t i = 0; i < BL_POOL_SIZE; i++)
  {
    for (size_t k = 0; k < BL_CHUNK_SIZE; k++)
    {
      if (buffers[i][k] != i + k)
      {
        return false;
      }
    }
  }

  return true;
}

bool bl_test_alloc_and_free(bl_allocator_t *bl_allocator)
{
  uint8_t *buffers[BL_POOL_SIZE];

  // alloc all chunks and fill by sample
  for (size_t i = 0; i < BL_POOL_SIZE; i++)
  {
    buffers[i] = bl_allocate(bl_allocator);

    for (size_t k = 0; k < BL_CHUNK_SIZE; k++)
    {
      buffers[i][k] = i + k;
    }
  }

  // free half of chunks (only odd)
  for (size_t i = 0, buf_idx = 1; i < BL_POOL_SIZE / 2; i++, buf_idx += 2)
  {
    bl_deallocate(bl_allocator, buffers[buf_idx]);
  }

  // alloc freed half of chunks
  for (size_t i = 0, buf_idx = 1; i < BL_POOL_SIZE / 2; i++, buf_idx += 2)
  {
    buffers[buf_idx] = bl_allocate(bl_allocator);

    for (size_t k = 0; k < BL_CHUNK_SIZE; k++)
    {
      buffers[buf_idx][k] = buf_idx - k;
    }
  }

  // compare with sample
  for (size_t i = 0; i < BL_POOL_SIZE; i++)
  {
    for (size_t k = 0; k < BL_CHUNK_SIZE; k++)
    {
      if ((i & 0x01) == 0)
      {
        if (buffers[i][k] != (uint8_t)(i + k))
        {
          return false;
        }
      }
      else
      {
        if (buffers[i][k] != (uint8_t)(i - k))
        {
          return false;
        }
      }
    }
  }

  return true;
}

/** @} */
